<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use App\Note;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CrudController extends Controller
{
    public function index() {
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        

        $note = Note::all();
        $no = 1;
        return view('barang.create', compact('note', 'no'));
    }

    public function create() {
        $no = 1;
        $response = Http::get('https://api.kawalcorona.com/indonesia/provinsi/');
        $data = $response->json();

        return view('barang.kopit', compact('data','no'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:5',
            'description' => 'required|min:5'
        ],
      [
          'title.required' => 'Harus diisi',
          'title.min' => 'Minimal 5 digit',
          'description.required' => 'Harus diisi',
          'description.min' => 'Minimal 5 digit',
       ]);

        $data = $request->all();
        // $data['slug'] = Str::slug($request->title);

        Note::create($data);
        
        alert()->success('Berhasil.','Data Note ditambahkan!');
        return redirect('barang');
    //   Note::create([
    //       'title' => $request->title,
    //       'slug' => \Str::slug(request('title')),
    //       'description' => $request->desciption,
    //   ]);
    }

    public function show($id)
    {
        $note = Note::find($id);
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('barang.show', compact('note'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
     {
         $note = Note::find($id);
        if(Auth::user()->role == 'user') {
            Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
       return view('barang.edit', compact('note'));
     }
     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|min:5',
            'description' => 'required|min:5'
        ]);
       
        Note::where('id',$id)->update([
            'title' => $request->title,
            'description' => $request->description
            ]);
        // $data = $request->all();
        // $data['slug'] = Str::slug($request->title);

        // $item = Note::findOrFail($id);

        // $item->update($data);

        // return redirect()->route('barang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id)
    {
        $item = Note::findOrFail($id);
        $item->delete();
        return redirect()->route('barang.create');
    }
}
