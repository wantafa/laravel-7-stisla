<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Title</h6>
            <p class="mb-0">{{ $note->title }}</p>
          </li>
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Deskripsi</h6>
            <p class="mb-0">{{ $note->description }}</p>
          </li>
        </ul>
      </div>